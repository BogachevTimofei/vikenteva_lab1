#include "mainwindow.h"
#include "ui_mainwindow.h"

int min = 0, max = 10;
QVector<QVector<int>> set(6);

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // создание таблицы
    QModelIndex index;
    model = new QStandardItemModel(5, 1, this);
    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    for(int row = 0; row < model->rowCount(); row++)
    {
        model->setHeaderData(row, Qt::Vertical, QString(char('A' + row)));
        index = model->index(row, 0);
        if(set[row].isEmpty())
            model->setData(index, "NULL");
    }
    model->setHeaderData(0, Qt::Horizontal, "Set");

    for(int i = min; i <= max; i++)
        set[5].append(i);
    // меню выпадающего списка
    {
        ui->editSelectSet->addItem("A", 0);
        ui->editSelectSet->addItem("B", 1);
        ui->editSelectSet->addItem("C", 2);
        ui->editSelectSet->addItem("D", 3);
        ui->editSelectSet->addItem("E", 4);

        ui->calcSelectSet->addItem("A", 0);
        ui->calcSelectSet->addItem("B", 1);
        ui->calcSelectSet->addItem("C", 2);
        ui->calcSelectSet->addItem("D", 3);
        ui->calcSelectSet->addItem("E", 4);
        ui->calcSelectSet->addItem("FULL", 5);

        ui->calcSelectSet_2->addItem("A", 0);
        ui->calcSelectSet_2->addItem("B", 1);
        ui->calcSelectSet_2->addItem("C", 2);
        ui->calcSelectSet_2->addItem("D", 3);
        ui->calcSelectSet_2->addItem("E", 4);
        ui->calcSelectSet_2->addItem("FULL", 5);

        ui->calcSelectSet_3->addItem("A", 0);
        ui->calcSelectSet_3->addItem("B", 1);
        ui->calcSelectSet_3->addItem("C", 2);
        ui->calcSelectSet_3->addItem("D", 3);
        ui->calcSelectSet_3->addItem("E", 4);
        ui->calcSelectSet_3->addItem("FULL", 5);

        ui->calcSelectSet_4->addItem("A", 0);
        ui->calcSelectSet_4->addItem("B", 1);
        ui->calcSelectSet_4->addItem("C", 2);
        ui->calcSelectSet_4->addItem("D", 3);
        ui->calcSelectSet_4->addItem("E", 4);
        ui->calcSelectSet_4->addItem("FULL", 5);

        ui->calcSelectAction->addItem("and", 0);
        ui->calcSelectAction->addItem("or", 1);
        ui->calcSelectAction->addItem("\\", 2);
        ui->calcSelectAction->addItem("in", 3);
        ui->calcSelectAction->addItem("▲", 4);

        ui->calcSelectAction_2->addItem("in", 0);
    }

    ui->tabWidget->setCurrentIndex(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString setToString(QVector<int> setVector)
{
    QString setString;
    if(setVector.isEmpty())
        setString = "NULL";
    else if(setVector.size() == max - min + 1)
        setString = "FULL";
    else
    {
        setString = "{" + QString::number(setVector[0]);
        for(int i = 1; i < setVector.size(); i++)
        {
            setString += "; " + QString::number(setVector[i]);
        }
        setString += "}";
    }
    return setString;
}

void MainWindow::on_spinMin_valueChanged(int arg1)
{
    min = arg1;
    ui->spinMax->setMinimum(arg1 + 1);

    set[5].clear();
    for(int i = min; i <= max; i++)
        set[5].append(i);

    QModelIndex index;
    for(int i = 0; i < set.size() - 1; i++)
    {
        for(int j = 0; j < set[i].size(); j++)
        {
            if(set[i][j] < min || set[i][j] > max)
            {
                set[i].remove(j);
            }
        }
        index = model->index(i, 0);
        model->setData(index, setToString(set[i]));
    }
}

void MainWindow::on_spinMax_valueChanged(int arg1)
{
    max = arg1;
    ui->spinMin->setMaximum(arg1 - 1);

    set[5].clear();
    for(int i = min; i <= max; i++)
        set[5].append(i);

    QModelIndex index;
    for(int i = 0; i < set.size() - 1; i++)
    {
        for(int j = 0; j < set[i].size(); j++)
        {
            if(set[i][j] < min || set[i][j] > max)
            {
                set[i].remove(j);
            }
        }
        index = model->index(i, 0);
        model->setData(index, setToString(set[i]));
    }
}

void MainWindow::on_editPushAdd_clicked()
{
    if(QString::number(ui->editLineIn->text().toInt()) == ui->editLineIn->text())
    {
        int currentSet = ui->editSelectSet->currentData().toInt();
        int value = ui->editLineIn->text().toInt();
        QModelIndex index;

        if(!set[currentSet].contains(value) && value >= min && value <= max)
        {
            set[currentSet].append(value);
            index = model->index(currentSet, 0);
            model->setData(index, setToString(set[currentSet]));
        }
    }
    ui->editLineIn->clear();
}

void MainWindow::on_editPushDelete_clicked()
{
    if(QString::number(ui->editLineIn->text().toInt()) == ui->editLineIn->text())
    {
        int currentSet = ui->editSelectSet->currentData().toInt();
        int value = ui->editLineIn->text().toInt();
        QModelIndex index;

        if(set[currentSet].contains(value) && value >= min && value <= max)
        {
            set[currentSet].remove(set[currentSet].indexOf(value));
            index = model->index(currentSet, 0);
            model->setData(index, setToString(set[currentSet]));
        }
    }
    ui->editLineIn->clear();
}

void MainWindow::on_editPushClear_clicked()
{
    model->setData(model->index(ui->editSelectSet->currentData().toInt(), 0), "NULL");
    set[ui->editSelectSet->currentData().toInt()].clear();
}

void MainWindow::on_calcPushClearAll_clicked()
{
    for(int i = 0; i < set.size(); i++)
    {
        model->setData(model->index(i, 0), "NULL");
        set[i].clear();
    }
}

void MainWindow::on_calcPushCalc_clicked()
{
    QVector<int> res;
    int set1, set2;
    set1 = ui->calcSelectSet->currentData().toInt();
    set2 = ui->calcSelectSet_2->currentData().toInt();
    switch(ui->calcSelectAction->currentData().toInt())
    {
        case(0):    // set1 and set2
        {
            for(int i = 0; i < set[set2].size(); i++)
            {
                if(set[set1].contains(set[set2][i]))
                {
                    res.append(set[set2][i]);
                    continue;
                }
            }
            ui->calcTextAnswer->setText(setToString(res));
            break;
        }
        case(1):    // or
        {
            for(int i = 0; i < set[set1].size(); i++)
            {
                res.append(set[set1][i]);
            }
            for(int i = 0; i < set[set2].size(); i++)
            {
                if(!res.contains(set[set2][i]))
                    res.append(set[set2][i]);
            }
            ui->calcTextAnswer->setText(setToString(res));
            break;
        }
        case(2):    // without(\)
        {
            for(int i = 0; i < set[set1].size(); i++)
            {
                res.append(set[set1][i]);
            }
            for(int i = 0; i < set[set2].size(); i++)
            {
                if(res.contains(set[set2][i]))
                {
                    res.remove(res.indexOf(set[set2][i]));
                    continue;
                }
            }
            ui->calcTextAnswer->setText(setToString(res));
            break;
        }
        case(3):    // in
        {
            bool check;
            for(int i = 0; i < set[set1].size(); i++)
            {
                check = true;
                for(int j = 0; j < set[set2].size(); j++)
                {
                    if(set[set2].contains(set[set1][i]))
                    {
                        check = false;
                        continue;
                    }
                }
                if(check) break;
            }
            if(check)
                ui->calcTextAnswer->setText("FALSE");
            else
                ui->calcTextAnswer->setText("TRUE");
            break;
        }
        case(4):    // ▲
        {
            for(int i = 0; i < set[set1].size(); i++)
            {
                res.append(set[set1][i]);
            }
            for(int i = 0; i < set[set2].size(); i++)
            {
                if(!res.contains(set[set2][i]))
                    res.append(set[set2][i]);
            }
            for(int i = 0; i < set[set2].size(); i++)
            {
                if(set[set1].contains(set[set2][i]))
                {
                    res.remove(res.indexOf(set[set2][i]));
                    continue;
                }
            }

            ui->calcTextAnswer->setText(setToString(res));
            break;
        }
    }
}

void MainWindow::on_calcPushCalc_2_clicked()
{
    int set3, value;
    set3 = ui->calcSelectSet_3->currentData().toInt();
    value = ui->calcValueIn->text().toInt();
    bool check = true;
    for(int i = 0; i < set[set3].size(); i++)
    {
        if(set[set3].contains(value))
        {
            check = false;
            break;
        }
    }
    if(check)
        ui->calcTextAnswer_2->setText("FALSE");
    else
        ui->calcTextAnswer_2->setText("TRUE");
}


void MainWindow::on_calcPushCalc_3_clicked()
{
    QVector<int> res;
    int set4;
    set4 = ui->calcSelectSet_4->currentData().toInt();
    for(int i = 0; i < set[5].size(); i++)
    {
        res.append(set[5][i]);
    }
    for(int i = 0; i < set[set4].size(); i++)
    {
        res.remove(res.indexOf(set[set4][i]));
        continue;
    }
    ui->calcTextAnswer_3->setText(setToString(res));
}

